from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import StaleElementReferenceException
import os
from os.path import expanduser
import re
import code


home_folder = expanduser('~')

# Comment/un-comment the following sections as desired to launch your chosen browser:
# Chrome (Assumes ChromeDriver can be found in your <home>/selenium/drivers folder):
driver = webdriver.Chrome(executable_path=home_folder + '/selenium/drivers/chromedriver')

# Firefox (Assumes GeckoDriver can be found in your <home>/selenium/drivers folder)::
# driver = webdriver.Firefox(executable_path=home_folder + '/selenium/drivers/geckodriver')

# Safari:
# https://developer.apple.com/documentation/webkit/testing_with_webdriver_in_safari
# Safaridriver should already be present on your system (High Sierra+): /usr/bin/safaridriver
# Run `safaridriver --enable` once from the terminal, before first use. Requires authentication.
# Note that the first time you use SafariDriver, you may get superfluous browser windows opening which also do not
# get shut down by driver.quit(). Subsequent launches of SafariDriver don't seem to result in this issue.
# driver = webdriver.Safari()
# The Safari window driven by SafariDriver tends to be quite small by default, so let's resize it to something
# more reasonable:
# driver.set_window_size(1024, 768)

# Internet Explorer (11):
# capabilities = {'browserName': 'internet explorer', 'platform': 'ANY'}
# driver = webdriver.Remote(desired_capabilities=capabilities, command_executor='http://192.168.110.72:4444/wd/hub')
# driver.set_window_size()

# Create a list to hold our output data:
results = []

# URL to navigate to:
URL = "https://chipublib-demo.bibliocms.com/blogs/"
driver.get(URL)
# Get every single DIV on the page:
divs = driver.find_elements(By.TAG_NAME, "div")
for div in divs:
    try:
        # Check if the DIV is actually visible:
        if div.is_displayed():
            # Get the HTML of the "top" level CSS node:
            outer_HTML = div.get_attribute("outerHTML")
            top_level_node = re.match("<div.*>", outer_HTML)
            # print(top_level_node[0])
            # print(60 * "-")
            # If you'd like to get the text of the DIV back, uncomment this:
            # print(div.text)
            # Get the font name, weight and pixel size of the DIV:
            # print(div.value_of_css_property("font-family"))
            # print(div.value_of_css_property("font-weight"))
            # print(div.value_of_css_property("font-size"))
            # print(80 * "-")
            # Format all our output and add it to our list:
            results.append({
                "css": top_level_node[0], 
                "font": div.value_of_css_property("font-family"), 
                "font_weight": div.value_of_css_property("font-weight"), 
                "font_size": div.value_of_css_property("font-size"),
                "separator": 150 * "-"
            })
    except StaleElementReferenceException:
        continue

# Uncomment this if you'd like to pause script execution:
# code.interact(local=dict(globals(), **locals()))

# Use Ctrl + D to resume execution, quiting the open WebDriver session:
driver.quit()

# Set up an HTML wrapper to format our output data:
wrapper = """
        <p><textarea rows="5" cols="100">{css}</textarea></p>
        <p>{font}</p>
        <p>{font_weight}</p>
        <p>{font_size}</p>
        <p>{separator}</p>
"""
# Remove the previous output HTML file if it exists:
if os.path.exists("output.html"):
    os.remove("output.html")
# And write our data to an output HTML file:
output_file = open("output.html", "a")
output_file.write("""
<html>
   <header>
        <a href=\"{0}\">{0}</a>
   </header>
<body>
""".format(URL))
for result in results:
    output_file.write(wrapper.format(**result))
output_file.write("""
</body>
</html>
""")
output_file.close()
